*** Settings ***
Library    Collections
Library    JSONLibrary
Library     os
Library     RequestsLibrary

*** Variables ***
${baseurl}  http://restcountries.eu/
*** Test Cases ***
Testcase1:
    ${json_obj}=    Load JSON From File		/opt/raone/JsonFiles/input.json
    ${name_value}=  get value from json  ${json_obj}    $.testcases[0].parameter
    should be equal  ${name_value[0]}   asm
Testcase2:
    create session  mysession   ${baseurl}
    ${response}=    get request     mysession   /rest/v2/alpha/IN
    ${json_object}=     to json     ${response.content}
    ${country_name}=    get value from json     ${json_object}     $.name
    should be equal  ${country_name[0]}  India
    ${borderscontent}=    get value from json     ${json_object}     $.borders
    should contain any  ${borderscontent[0]}  AFG     BGD     BTN     CHN
****** Keywords ***

